#!/bin/bash

if [ $# -lt 2 ]; then
  echo "Usage: $(basename $0) DESTINATION_DIR CHART..." >&2
  exit 1
fi

DESTINATION_DIR=$1
shift
CHARTS_TO_PACKAGE=$@
SECRET_KEYRING=./gitlab.secring.gpg
PUBLIC_KEYRING=./gitlab.pubring.gpg
PUBLIC_EXPORTED_KEYRING=./gitlab.pubring-exported.gpg
SIGNING_GPG_KEY=${SIGNING_GPG_KEY:-'GitLab, Inc. Helm charts'}

if [ "$SIGN_CHARTS" == "true" ]; then
  # Check environment variables.
  if [ -z "$SIGNING_SECRET_AWS_ACCESS_KEY_ID" ]; then
    echo "error: SIGNING_SECRET_AWS_ACCESS_KEY_ID is not set." >&2
    exit 1
  fi
    if [ -z "$SIGNING_SECRET_AWS_SECRET_ACCESS_KEY" ]; then
    echo "error: SIGNING_SECRET_AWS_SECRET_ACCESS_KEY is not set." >&2
    exit 1
  fi
    if [ -z "$SIGNING_GPG_PASSPHRASE" ]; then
    echo "error: SIGNING_GPG_PASSPHRASE is not set." >&2
    exit 1
  fi

  # GPG keys for chart signing and verification.
  echo "Copying GPG keys."
  AWS_ACCESS_KEY_ID="$SIGNING_SECRET_AWS_ACCESS_KEY_ID" AWS_SECRET_ACCESS_KEY="$SIGNING_SECRET_AWS_SECRET_ACCESS_KEY" aws s3 cp s3://gitlab-helm-charts-sig/$(basename $SECRET_KEYRING) .
  AWS_ACCESS_KEY_ID="$SIGNING_SECRET_AWS_ACCESS_KEY_ID" AWS_SECRET_ACCESS_KEY="$SIGNING_SECRET_AWS_SECRET_ACCESS_KEY" aws s3 cp s3://gitlab-helm-charts-sig/$(basename $PUBLIC_KEYRING) .
  gpg --import $PUBLIC_KEYRING
  gpg --export > $PUBLIC_EXPORTED_KEYRING

  echo "Packaging and signing helm charts using signing key '$SIGNING_GPG_KEY'." ;
  helm package \
      --destination $DESTINATION_DIR \
      --sign \
      --passphrase-file "$SIGNING_GPG_PASSPHRASE" \
      --key "$SIGNING_GPG_KEY" \
      --keyring $SECRET_KEYRING \
      $CHARTS_TO_PACKAGE

  echo "Verifying helm charts."
  for chart in $DESTINATION_DIR/*.tgz; do
      helm verify --keyring $PUBLIC_EXPORTED_KEYRING $chart
  done

  # Cleanup secrets.
  rm -f $SECRET_KEYRING $PUBLIC_KEYRING $PUBLIC_EXPORTED_KEYRING
else
  echo "Packaging helm charts without signing." ;
  helm package --destination $DESTINATION_DIR $CHARTS_TO_PACKAGE
fi

echo "Listing helm chart files."
find $DESTINATION_DIR -name "*.tgz*"
